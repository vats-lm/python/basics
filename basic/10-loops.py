
from __future__ import print_function

# Loops
'''
* while loop
* for loop
* nested loop
'''

# Loop control statements
'''
* break statement
* continue statement
* pass statement
'''

# while loop
count = 0
while count < 5:
    # This is how we can print multiple type of variables.
    print(count, " is  less than 5")
    count += 1
else:  # loop support 'else' which executes when condition in loop become false.
    print(count, " is not less than 5") # For unknown reason, braces (that are here after 'print' statement) are also printed on terminal.


# for loop - for in
for letter in 'Python':     # First Example
   print('Current Letter :', letter)

fruits = ['banana', 'apple',  'mango'] # Array variable
for fruit in fruits:        # Second Example
   print('Current fruit :', fruit)

print(range(len(fruits)))
for index in range(len(fruits)):  # range will let for loop to iterate from 0 to [value passed to range]
    print('Current fruit is ', fruits[index])
else: print("All elements of array passed to for loop are processed.")


# nested loops
for i in range(2, 101):
    j = 2
    while j <= i / j:
        if not (i % j): break
        j += 1
    if j > i / j:
        print(i, " is a prime number")


# pass
'''
It is used when a statement is required syntactically but you do not want any command or code to execute.

The pass statement is a null operation; nothing happens when it executes. The pass is also useful in places where your code will eventually go, but has not been written yet (e.g., in stubs for example)
'''
for letter in 'Python':
   if letter == 'h':
      pass
      print('This is pass block')
   print('Current Letter :', letter)
