
# Types of numbers:
x = 1    # int
y = 2.8  # float
z = 1j   # complex

# Use 'type(..)' to get type of the variable:
print(type(z))

# Int, or integer, is a whole number, positive or negative, without decimals, of unlimited length.

# Float, or "floating point number" is a number, positive or negative, containing one or more decimals.
# Float can also be scientific numbers with an "e" to indicate the power of 10.
z = -87.7e100
print(z)

# Complex numbers are written with a "j" as the imaginary part:
x = 3+5j
y = 5j
z = -5j
