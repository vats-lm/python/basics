
# One line if else statement:
a = 200
b = 33
if a > b: print("A")
else: print("B")

'Yes' if "fruit" == 'Apple' else 'No'
# print("A") if a > b else print("B")
