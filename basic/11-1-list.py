

thislist = ["apple", "banana", "cherry"]

# print each list element
for e in thislist:
    print(e)

# check if element is in list
if 'apple' in thislist:
    print("apple is in list")

# list length
print(len(thislist))

# add in element in list
thislist.append("orange")

# del
# diff from remove function that needs an item value to remove instead of index
del thislist[0]
print(thislist)
'''
del thislist
# this will cause an error because "thislist" no longer exists.
print(thislist)
'''

# list constructor
# note the double round-brackets
thislist = list(("apple", "banana", "cherry", "cherry"))
print(thislist)
