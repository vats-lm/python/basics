
# We can also use 'python' command to run the python code directly on terminal.
# Follow below listed steps to run python on terminal
'''
type : python
type : print("hello")
terminal will print : hello
to exit from the terminal, type : exit()
'''
"""
You can also use python shell (come with an python installer) to run the python code.
"""
print("Thats it!")