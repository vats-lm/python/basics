
a = 5

# if
print("before if block")
if a != 5:
    print("with in if block")
    print(a)
print("after if block")

# if else
print("before if else block")
if a != 5:
    print("with in if block")
    print(a)
else:
    print("with in else block")
print("after if else block")

# if elif else
print("before elif else block")
if a != 5:
    print("with in if block")
    print(a)
elif a == 5:
    print("with in elif block")
    print(a)
else:
    print("with in else block")
print("after elif else block")

# nested if
print("before nested if block")
print("1")
if a == 5:
    print("with in if block")
    if a != 5: print("with in if-if block")  # one line if
    else: print("with in if else block")  # one line else

print("2")
if a == 5:
    print("with in if block")
    if a != 5: print("with in if-if block")
else: print("with in if else block")  # this is else of first if based on no tab
print("after nested if block")


