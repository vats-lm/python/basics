
# set is created by using curly brackets
thisset = {"banana", "apple", "cherry"}
# Once a set is created, you cannot change its items, but you can add new items using add or update(add more than one element with providing list to this method) method.

# ordered ?
print(thisset)

# changable ?
'''
* Not possible
* Can be acheived by remove an element and add that element with updated value.
'''

# indexed ?
'''
print(thisset[1]) # gives an error:
TypeError: 'set' object does not support indexing
'''

# duplicate ?
thisset = {"apple", "banana", "cherry", "cherry"}
print(thisset)

# accessable ? - not directly but
# using loop:
for x in thisset:
  print(x)
# using in
print("banana" in thisset)

# removing(remove(..)) a element in set raise an error(e.g. thisset = {} \n thisset.pop()) but
# discarding(discard(..)) a element doesn't raise an error

