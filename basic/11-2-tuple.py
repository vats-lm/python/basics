
# tuples are written with round brackets
thistuple = ("apple", "banana", "cherry")

# ordered ?
print(thistuple)

# changable ?
'''
thistuple[1] = "blackcurrant" # gives an error:
TypeError: 'tuple' object does not support item assignment
'''

# indexed ?
print(thistuple[1])

# duplicate ?
thistuple = ("apple", "banana", "cherry", "cherry")
print(thistuple)

# all methods of list are available on tuple except the methods that delete elements like del, remove

# count the number of occurance of element
print(thistuple.count("cherry"))

# get index of element by its value
print(thistuple.index("cherry"))

# tuples can be created with tuple constructor
thistuple = tuple(("a", "b", "c"))
