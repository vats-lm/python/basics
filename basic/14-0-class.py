
# Class
'''
* Python is an object oriented programming language.
* Almost everything in Python is an object, with its properties and methods.
* A Class is like an object constructor, or a "blueprint" for creating objects.
'''

class Person:
    age = 0

child = Person()
child.age = 5 
print(child.age)
