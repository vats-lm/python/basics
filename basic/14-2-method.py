
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    # first parameter can be any name used to access the current object in class.
    def myfunc(self):
        # self used: just to satisfy the Python linter in VS Code.
        print("Hello my name is " + self.name)


p1 = Person("John", 36)
p1.myfunc()

del p1.age
# p1.age # gives error
del p1
