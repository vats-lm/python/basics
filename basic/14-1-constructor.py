from __future__ import print_function

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self._age = age  # just to prove that accessing var with self makes it class variable

johnny = Person("Johnny", 18)
print(johnny.name, "is", johnny.age, "years old.")

class Car:
    def __init__(this, name): # first param passed to constructor is a object name in class of a object created.
        this.name = name

ferrari = Car("ferrari")
print(ferrari.name)
