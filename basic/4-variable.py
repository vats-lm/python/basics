
"""
* Unlike other programming languages, Python has no command for declaring a variable.
* A variable is created the moment you first assign a value to it.
* A variable name
* * must start with a letter or the underscore character
* * cannot start with a number
* * can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
* Variable names are case-sensitive (age, Age and AGE are three different variables)
"""

x = 5
y = 6

if(x > y):
    print("x is greater than y")
else:
    print("x is less than y")

# Assigning another type of value is also fine
x = "string value"
print(x + " concatenated.")

# Concatenating 'String' with 'Int' cause an error
# print(x + y)