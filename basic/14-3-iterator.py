

class MyNumbers:

    def __iter__(self):
        self.a = 1
        return self

    def __next__(self):
        self.a += 1
        if self.a == 4: raise StopIteration
        return self.a - 1


myclass = MyNumbers()
myiter = iter(myclass)

print(next(myiter))
print(next(myiter))

for i in myclass:
    print(i)

# string, list, set etc are all iterators and implement __iter__ and __next__ methods.
