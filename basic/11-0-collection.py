
# Four collection data types
'''
                Ordered     Changeable      Indexed     Duplicate
List              Yes          Yes            Yes          Yes
Tuple             Yes          No             Yes          Yes
Set               No           No             No           No
Dictionary        No           Yes          Key Yes    Key No, Value Yes

'''
