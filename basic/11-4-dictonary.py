
from __future__ import print_function

# dictionaries are written with curly brackets, and they have keys and values.
thisdict = {
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964,
    1: 2,
    1.1: 3
}

# ordered ?
print(thisdict)

# changable ?
thisdict[1] = 4
print(thisdict)

# indexed and accesable ?
# using keys

# duplicate ?
thisdict = {
    "brand": "Ford",
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964,
    1: 2,
    1.0: 1964
}
print(thisdict)

# print all keys
for key in thisdict:
    print(key)

# print all values of dictionary
for value in thisdict.values():
    print(value)

# print all keys and values of dictionary
for key, value in thisdict.items():
    print(key, value)

# check if key in dictionary
print("brand" in thisdict)

# dict constructor
thisdict = dict(brand="Ford")
# dict(brand="Ford", 1=2, 2=3) # throws: SyntaxError: keyword can't be an expression
print(thisdict)
