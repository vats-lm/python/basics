
"""
* Braces ('(', ')') can be used with if condition.
* Colon (':') must be used at the end of 'if' and 'else'.
* Identation is mandatory.
* "elif" is the python of using else-if statements.
"""

if 5 > 2:
    print("if without braces")

if(5 > 2):
    print("if with braces")

if(5 < 2):
    print "print without braces"
else:
    print("print with braces")

if 4 == 0:
    print "Not possible"
elif 4 > 7:
        print "Also not possible"
else:
    print "Default"

