
'''
* Use single or double quote for defining string.
* Strings in Python are arrays of bytes representing unicode characters.
* Python does not have a character data type, a single character is simply a string with a length of 1.
* Square brackets can be used to access elements of the string.
'''

a = " This is: string "
print(a[1])
print(a[1:3])  # 3 position not included in the substring.
print(a.strip())  # remove trailing spaces like trim function.
# len(..) is not a string class function but used to get length of string.
print(len(a))
print(a.lower() + a.upper())
print(a.replace("is is:", "ese are"))
print(a.split(":"))
sa = a.split(":")
print(sa[0])
print(sa[1])
