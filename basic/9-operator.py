
# Python divides the operators in the following groups:
'''
Arithmetic operators
Assignment operators
Comparison operators
Logical operators
Identity operators
Membership operators
Bitwise operators
'''

# Only non-familiar is listed (I am familiar with java and c++)

# Arithmetic operators
'''
**	Exponentiation
//	Floor division
'''
'''
* In python 3 div behaves like floor div completely
* In python 2 div behaves like floor dev only if both operands are integer
'''
a = 10
b = 3
ab = a ** b  # 1000
print(ab)

a = 9
b = 2
adb = a / b  # 4
afdb = a // b  # 4
print(adb)
print(afdb)

a = -9  # Or b = -2
adb = a / b  # -5
afdb = a // b  # -5
print(adb)
print(afdb)


# Assignment operators
a = 10
b = 2
a **= b # 100
print(a)
a //= b # 50
print(a)


# Comparison operators
a = "abc"
b = "abc"
print(a == b)  # True
print(a <> b)  # False * <> is same as !=


# Logical Operators - and, or, not
a = True
b = False
if (a and b):
    print("a && b")
elif (a or b):
    print("a || b")
elif not (a and b):
    print("!(a && b)")


# Identity operators - is, is not
a = 20
b = 20

if (a is b):
   print "Line 1 - a and b have same identity"
else:
   print "Line 1 - a and b do not have same identity"
print(id(a))
print(id(b))
if (id(a) == id(b)):
   print "Line 2 - a and b have same identity"
else:
   print "Line 2 - a and b do not have same identity"

b = 30
print(id(b))
if (a is b):
   print "Line 3 - a and b have same identity"
else:
   print "Line 3 - a and b do not have same identity"

if (a is not b):
   print "Line 4 - a and b do not have same identity"
else:
   print "Line 4 - a and b have same identity"


# Membership operators - in, not in
a = 10
b = 20
list = [1, 2, 3, 4, 5]

if (a in list):
   print "Line 1 - a is available in the given list"
else:
   print "Line 1 - a is not available in the given list"

if (b not in list):
   print "Line 2 - b is not available in the given list"
else:
   print "Line 2 - b is available in the given list"

a = 2
if (a in list):
   print "Line 3 - a is available in the given list"
else:
   print "Line 3 - a is not available in the given list"


# Bitwise Operators - &, |, ^, ~, <<, >>
'''
a = 0011 1100

b = 0000 1101

-----------------

a&b = 0000 1100   Binary AND

a|b = 0011 1101   Binary OR

a^b = 0011 0001   Binary XOR

~a  = 1100 0011   Binary Ones Complement

# << Binary Left Shift
a << 2 = 240 (means 1111 0000)

# >> Binary Right Shift
a >> 2 = 15 (means 0000 1111)
'''

# Operators Precedence
'''
1	**          Exponentiation (raise to the power)

2	~ + -       Complement, unary plus and minus (method names for the last two are +@ and -@)

3	* / % //    Multiply, divide, modulo and floor division

4	+ -         Addition and subtraction

5	>> <<       Right and left bitwise shift

6	&           Bitwise 'AND'

7	^ |         Bitwise exclusive `OR' and regular `OR'

8	<= < > >=   Comparison operators

9	<> == !=    Equality operators

10	= %= /= //= -= += *= **=    Assignment operators

11	is is not   Identity operators

12	in not in   Membership operators

13	not or and  Logical operators

'''
