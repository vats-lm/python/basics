
# Lamda
'''
* Small anonymous function.
* Can take any number of arguments, but can only have one expression.
* syntax: lambda param : return
'''

sum = lambda a, b : a + b
print(sum(10, 15))


def myfunc(n):
  return lambda a: a * n

mydoubler = myfunc(2)
mytripler = myfunc(3)
myHexer = myfunc(6)

print(mydoubler(11))
print(mytripler(11))
print(myHexer(11))
