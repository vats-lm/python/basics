
# def is used to define function.
def printHello():
    print("hello")

printHello()

def sum(a, b = 0):
    return a + b

print(sum(5, 10))
print(sum(10)) # default param works.