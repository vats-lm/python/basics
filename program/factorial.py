from __future__ import print_function

print("Enter number: ")
num = input()

def findFact(n):
    if n < 2: return 1
    return n * findFact(n - 1)

print(num, "factorial is", findFact(num))