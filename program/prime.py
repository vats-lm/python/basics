
from __future__ import print_function

print("Enter number: ")
num = input()   # 29

if num < 2:  # False
    print(num, "is not a prime number.")
    exit()

j = 2
while j <= num / j:  # 2 <= 14, 3 <= 9, 4 <= 7, 5 <= 5, 6 <= 4 (False)
    if not (num % j):
        break
    j += 1

if j > num / j:  # 6 > 29/6 i.e 4 (True)
    print(num, "is a prime number.")  # 29 is a prime number.   
else:
    print(num, "is not a prime number.")
