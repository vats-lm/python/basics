

powFunc = lambda x: x ** 2
m = map(powFunc, range(10))
print(list(m))

y = [x ** 2 for x in range(10)]
print(y)

z = lambda y: [x for x in range(y)]
