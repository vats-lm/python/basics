

# Problem
def f(a, L=[]):
    L.append(a)
    return L

print(f(1)) # L evaluated to [] i.e empty list but it is a valid mutable object
print(f(2))  # L already assigned when f(1) is called so L is [1] intially
# Now L is [1, 2]
print(f(3))
# Now L is [1, 2, 3]

'''
* The default value is evaluated only once. This makes a difference when the default is a mutable object such as a list, dictionary, or instances of most classes.
* For example, the above function (f(a,L)) accumulates the arguments passed to it on subsequent calls:
'''

# Expected output
'''
[1]
[2]
[3]
'''
# See solution file.

