

# Problem
ar = [1, 10, 100]
for a in ar:
    print('Processing ' + str(a) + ' from ' + str(ar))
    if a > 9:
        ar.insert(0, a - 1)
        print(ar)
        input("wait..")

'''
Here, every time insert method is called on a 'ar' list, the index (maintained internally by for loop) will be the index of item '10' in 'ar' list.
Reason:
- When first time 'if' is true then the index with in loop should be '1' i.e a index of item '10'.
- When new item i.e '10 - 1 = 9' will be inserted in a list then the item '10' will be shifted at position '2' in a list.
- Loop continue to increment the internal index and that should be '2' after insertion and same item is processed again because the list will be changed to [9, 1, 10, 100] after insertion.
- Hence infinite loop.
'''

