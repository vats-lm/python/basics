

# Solution
def f(a, L=None):
    if L is None:
        L = []
    L.append(a)
    return L

print(f(1))  # L is evaluate to None as default if none is passed as 2nd arg.
# L is now [1]
print(f(2))
# L is now [2]
print(f(3))
# L is now [3]

# We get the expected output.