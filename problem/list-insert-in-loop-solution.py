

# Solution 1 (Working)
print('------ Solution 1 ----------')
ar = [1, 10, 100]
for a in ar[:]:
    print('Processing ' + str(a) + ' from ' + str(ar))
    if a > 9:
        ar.insert(0, a - 1)
        input("wait..")

'''
Here 'ar[:]' will return a copy of 'ar' list which results the same number of iteration as the number of element in a list at the time of copy.
'''

# Solution 2 (Failed to implement)
print('------ Solution 2 ----------')
ar = [1, 10, 100, 1000]
indexRange = range(len(ar))  # range(0, 4)
for index in indexRange: # index 0, 1, 2, 3
        a = ar[index]
        print('Processing ' + str(a) + ' from ' + str(ar))
        if a > 9:
                ar.insert(0, a - 1)
                index += 1  # This line has no effect on the value of index.
                indexRange = range(len(indexRange) + 1)
                input("wait..")
'''
I failed to change the value of 'index' hence the number of iteration in loop can't be change this way.
'''

# Solution 3 (Working)
print('------ Solution 3 ----------')
ar = [1, 10, 100, 1000]
index = 0
arLen = len(ar)
while index < arLen:
    a = ar[index]
    print('Processing ' + str(a) + ' from ' + str(ar))
    if a > 9:
        ar.insert(0, a - 1)
        arLen += 1
        index += 1
    index += 1
    input("wait..")

'''
* while loop is used to achieve the dynamically changine condition in loop.
* used index that generally increment by one but increment by two if insertion.

Pros:
* Require less number of comparision to iterate over array.
- Remember in 'for in' loop, there is a need to check if a is present in 'ar[:]' or not. This need is removed by incrementing 'arLen' and 'index' by one after insertion and less than comparision in while loop.
* Memory Efficient than Soution 1
- We are not reserving a block of memory equavalent to the list size.
- Only two variables (i.e 'index' and 'arLen') are used to solve the problem.

Cons:
- Large
- Not easily readable
'''

